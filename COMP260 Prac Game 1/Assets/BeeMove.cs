﻿using UnityEngine;
using System.Collections;

public class BeeMove : MonoBehaviour {
	public float speed = 4.0f;
	public float turnSpeed = 180.0f;
	public Transform target1;
	public Transform target2;
	public Vector2 heading = Vector3.right;
    public float minSpeed, maxSpeed;
    public float minTurnSpeed, maxTurnSpeed;
    public ParticleSystem explosionPrefab;

    void OnDestroy()
    {
        ParticleSystem explosion = Instantiate(explosionPrefab);
        explosion.transform.position = transform.position;
        Destroy(explosion.gameObject, explosion.duration);
    }

	// Use this for initialization
	void Start () {
        target1 = FindObjectOfType<PlayerMove>().transform;
        target2 = FindObjectOfType<Player2Move>().transform;
        heading = Vector2.right;
        float angle = Random.value * 360;
        heading = heading.Rotate(angle);

        // set speed and turnSpeed randomly 
        speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
        turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed,
                                 Random.value);

    }
	
	// Update is called once per frame
	void Update () {


        float dist1 = Vector3.Distance(target1.position, transform.position);
        float dist2 = Vector3.Distance(target2.position, transform.position);
        
		Vector2 direction;
        if ( dist1 > dist2) {
            direction = target2.transform.position - transform.position;
        }
        else {
            direction = target1.transform.position-transform.position;
        }

        float angle = turnSpeed * Time.deltaTime;

        if (direction.IsOnLeft(heading))
        {
            heading = heading.Rotate(angle);
        }
        else
        {
            heading = heading.Rotate(-angle);
        }
        transform.Translate(heading * speed * Time.deltaTime);
    }

    void OnDrawGizmos() {
		Gizmos.color = Color.red;
		Gizmos.DrawRay (transform.position, heading);

		Gizmos.color = Color.yellow;

        if (target1 != null)
        {
            Vector2 direction = target1.position - transform.position;
            Gizmos.DrawRay(transform.position, direction);
        }
        if (target2 != null)
        {
            Vector2 direction = target2.position - transform.position;
            Gizmos.DrawRay(transform.position, direction);
        }
    }
}