﻿using UnityEngine;
using System.Collections;

public class Player2Move : MonoBehaviour {

	public float maxSpeed = 5.0f;
	public Vector2 velocity;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		Vector2 direction;
		direction.x = Input.GetAxis ("HorizontalPlayer2");
		direction.y = Input.GetAxis ("VerticalPlayer2");



		Vector2 velocity = direction * maxSpeed;

		transform.Translate(velocity * Time.deltaTime);
	}
}
