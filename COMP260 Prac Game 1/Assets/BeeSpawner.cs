﻿using UnityEngine;
using System.Collections;

public class BeeSpawner : MonoBehaviour {
    public BeeMove beePrefab;
    public int nBees = 50;
    public float xMin, yMin;
    public float width, height;

    public float minBeePeriod, maxBeePeriod;

    private float nextSpawn = 0;

    private int beeCounter;

	// Use this for initialization
	void Start () {
        for (int i = 0; i < nBees; i++)
        {
            BeeMove bee = Instantiate(beePrefab);
            bee.transform.parent = transform;
            bee.gameObject.name = "Bee" + i;
            float x = xMin + Random.value * width;
            float y = yMin + Random.value * height;
            bee.transform.position = new Vector2(x, y);
            beeCounter++;
        }
	}
	
	// Update is called once per frame
	void Update () {

        nextSpawn -= Time.deltaTime;
	
        if(nextSpawn <= 0)
        {
            nextSpawn = Random.Range(minBeePeriod, maxBeePeriod);

            BeeMove bee = Instantiate(beePrefab);
            bee.transform.parent = transform;
            beeCounter++;
            bee.gameObject.name = "Bee" + beeCounter;
            float x = xMin + Random.value * width;
            float y = yMin + Random.value * height;
            bee.transform.position = new Vector2(x, y);

        }
    }

    public void DestroyBees(Vector2 centre, float radius){

        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            Vector2 v = (Vector2)child.position - centre;
            if (v.magnitude <= radius)
            {
                Destroy(child.gameObject);
            }
        }
    }
}
